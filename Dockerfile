# Use Ubuntu as the base image
FROM ubuntu:latest

# Set environment variables for Java
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
ENV PATH=$PATH:$JAVA_HOME/bin

# Install necessary packages: Git, Maven, and OpenJDK 11
RUN apt-get update && \
    apt-get install -y git maven openjdk-11-jdk

# Confirm Java, Maven, and Git installations
RUN java -version
RUN mvn -version
RUN git --version

# Define a working directory (you can change this as needed)
WORKDIR /app

# Define a default command or entrypoint if necessary
CMD ["/bin/bash"]